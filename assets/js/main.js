(function($) {
    let app = {
        settings : {
            url: "http://demo1183916.mockable.io/anuncio"
        },
        init: () => {
            app.get(app.settings.url);
            app.submit();
        },
        get: (url) => {
            $.ajax({
                url: url,
                type: 'GET',
                success: function(response) {
                    const anuncios = response.result;
                    const template = $('#template').html();

                    anuncios.forEach(function (value) {
                        Mustache.parse(template);
                        const rendered = Mustache.render(template, {
                            name: value.name,
                            description: value.description,
                            type: value.type,
                            place: value.place,
                            images: value.images,
                            value: value.value
                        });
                        $('#target').html(rendered);
                        if($('body').hasClass('home')) {
                            app.map.init(value.place[0].address);
                        }
                    });
                }
            });
        },
        submit: () => {
            const form = $('#form');
            const action = form.attr('action');
            const method = form.attr('method');
            const data = form.serialize();

            $(form).on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    url: action,
                    type: method,
                    data: data,
                    success: function(response) {
                        console.log(response);
                    },
                    error: function(response) {
                        console.log(response);
                    }
                });
            });
        },
        map: {
            init: (address) =>{
                geocoder = new google.maps.Geocoder();
                let latlng = new google.maps.LatLng(-34.397, 150.644);
                let mapOptions = {
                  zoom: 17,
                  center: latlng
                }
                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                app.map.codeAddress(address);
            },
            codeAddress: (myAddress) => {
                let address = myAddress;
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == 'OK') {
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location
                        });
                    }
                });
            }
        }
    }
    
    $(document).ready(function() {
        app.init();
    });

})(jQuery); 